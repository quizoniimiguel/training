/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
public class Fibonacci {
    public static void main (String [] args){
        
        int PreviousNum = 0;
        int NextNum = 1;
        
        int Fibonacci = 0;
        int Sum = 0;
                
        for (int Counter = 1; Counter <= 20; Counter++){
            
            Fibonacci = PreviousNum + NextNum;
            NextNum = Fibonacci;
            PreviousNum += NextNum;
            
            System.out.print(Fibonacci + " ");
        }
        
    }
    
}
