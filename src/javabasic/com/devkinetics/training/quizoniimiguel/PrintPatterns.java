/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
import java.util.Scanner;
public class PrintPatterns { 
    static int size = 8;
    static int row,col;
    public static void main(String[]args){ 
        printPatternA();
        printPatternB();
        printPatternC();
        printPatternD();
        printPatternX();
    }        
    public static void printPatternA(){
        for (int row = 1; row <= size; ++row){
            for (int col = 1; col <= row; ++col){
                System.out.print("# ");
            }
            System.out.println();
        }
    System.out.println(); 
    }
    
    public static void printPatternB(){
        //row = 1;true;
        for (int row = 1; row <= size; ++row){
            for (int col = 1; row + col <=size + 1; col++){
                System.out.print("# ");
            }
            System.out.println();
        }
        System.out.println();
    }
    
    public static void printPatternC(){
        for (int row = 1; row <= size+1; row++){            
            for (int col = 1; col <= size+1;col++){
                if (col <= row){
                    System.out.print(" ");
                } else {
                    System.out.print("# ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }
    
    public static void printPatternD(){
        for (int row = 8; row >= 1; row--){            
            for (int col = 1; col <= 8;col++){
                if (col >= row){
                    System.out.print("# ");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }
    
    public static void printPatternX(){
        for (int row = 1; row <= size; ++row){
            for (int col = 1; col <= row; ++col){
                System.out.print(row + " ");
            }
            System.out.println();
        }
    System.out.println(); 
    }
    
}