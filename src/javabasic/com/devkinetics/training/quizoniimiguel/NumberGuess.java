/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
import java.util.Scanner;
public class NumberGuess {
    public static void main (String[]args){
        
        int secretNumber = (int)(Math.random()*100);
        boolean done = false;
        int counter = 0;
        int guess = 0;
        
        Scanner in = new Scanner (System.in);
        System.out.println("Key in your guess: ");
        //System.out.println(secretNumber);
        while (!done){
            guess = in.nextInt();
            counter++;
            if (secretNumber < guess){
                System.out.println("Try Lower!");
            } else if (secretNumber > guess){
                System.out.println("Try Higher!");
            } else {
                System.out.println("You got it in "+counter+" trials.");
                done = true;
            }
            
        }
        
    }
}
