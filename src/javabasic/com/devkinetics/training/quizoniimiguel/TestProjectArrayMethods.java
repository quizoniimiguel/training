/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
import java.util.Scanner;
public class TestProjectArrayMethods {
    
    //Global Variables Here
    public static int [] inputs;
    public static int numInputs;
        
        public static void main(String[]args){
            readInput();
            System.out.println("The sum of the input is: "+countInput());
            System.out.println("The inputs are: "+outputValues());
        }
        
        public static void readInput(){
            
            Scanner in = new Scanner(System.in);
            System.out.print("Enter the number of inputs: ");
            numInputs = in.nextInt();
            inputs = new int[numInputs];
            int testMethod = 0;
            
            for (int idx = 0; idx < numInputs; idx++){
                System.out.print("Enter input number "+(idx+1)+": ");
                inputs[idx] = in.nextInt();
                testMethod = countInput(inputs[idx]);
            }            
        }
        
        public static int countInput(int additives){
            int sum = 0;
            sum += additives;            
        }
        
        public static int outputValues(){
            for (int idx = 0; idx < numInputs; idx++){
                return(inputs[idx]);
            }
            return(inputs[0]);
        }
}