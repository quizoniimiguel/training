/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */

import java.util.Scanner;

public class TestPalindromicWord {
    public static void main (String [] args){
        
        String inStr;
        int StrLen = 0;
        String revStr = "";
        int trueLength = 0;
        
        
        Scanner in = new Scanner (System.in);
        System.out.print("Enter a palindromic word: ");
        inStr = in.next();
        
        trueLength = inStr.length() - 1;
        
        while (trueLength >= 0){              
            revStr = revStr+inStr.charAt(trueLength);         
            trueLength--;               
        }      
        boolean result = inStr.equalsIgnoreCase(revStr);
        System.out.println(inStr + " is not a palindrome "+result);
        if (result) {
            System.out.println(inStr+" is a palindrome");
            
        }else{
        System.out.println(inStr + " is not a palindrome ");
        }
    }
}
