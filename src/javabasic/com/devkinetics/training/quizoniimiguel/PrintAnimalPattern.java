/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
public class PrintAnimalPattern {
    public static void main (String[]args){
       System.out.println("              '  '");
       System.out.println("               --");
       System.out.println("              (\u00a9\u00a9)");
       System.out.println("  /============\\/");
       System.out.println(" / ||  %%%%  ||");
       System.out.println("*  ||--------||");
       System.out.println("   \u00a5\u00a5        \u00a5\u00a5");
       System.out.println("   \"\"        \"\" ");
    }
}
