/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
public class SumDigits {
    public static void main (String[]args){
        
        String inputDigit;
        int inDigiLen;
        char inDigit;
        int sum = 0;
        
        if (args.length != 1){
            System.err.println("Usage: java SumDigits InputDigit");
            return;
        }
        
        inputDigit = args[0];
        inDigiLen = args[0].length();
        
        System.out.print("The sum of digits = ");
        
        for (int idx = 0; idx < inDigiLen; idx++){
            
            inDigit = inputDigit.charAt(idx);
            int indiDigit = inDigit - '0';
            System.out.print(indiDigit);
            if (idx < inDigiLen){
                System.out.print("+ ");
            } else {
                System.out.print("= ");
            }
            
            sum += indiDigit;
        }
        
        System.out.print(sum);
    }
}
