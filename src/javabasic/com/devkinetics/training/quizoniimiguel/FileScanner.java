/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;


public class FileScanner {
    public static void main(String[] args)
            throws FileNotFoundException { //Needed for file operation
        int num1;
        double num2;
        String name;
        double sum;
        
        //Setup a Scanner to read from a text file
        Scanner in = new Scanner(new File("in.txt"));
        num1 = in.nextInt();    //use nextInt() to read int
        num2 = in.nextDouble(); //use nextDouble() to read double
        name = in.next();       // use next() to read String
        sum = num1+num2;
        //Display
        System.out.print("The integer read is "+num1);
        System.out.println("\nThe floating point number read is "+num2);
        System.out.println("The String read is "+name);
        
        
    }
}
