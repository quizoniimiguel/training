/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
public class GradesHistogram {
    public static int[] grades;
    //Declare an int array of grades , to be allocated later
    public static int[] bins = new int[10];
    //Declare and allocare an int array for histogram bins.
    //10 bins for 0-9, 10 - 19 ,... 90-100
    
    public static void main (String [] args){
        readGrades("grades.in");
        computeHistogram();
        printHistogramHorizontal();
        printHistogramVertical();
    }
    //Read the grades from filename store in grades array.
    //Assume that the inputs are valid.
    public static void readGrades(String filename){
        
    }
    //Bases on "grades" array, populate the "bins" array
    public static void computeHistorgram(){
        
    }
    //print histogram based on the "bins" array.
    public static void printHistogramHorizontal(){
        
    }
    //print histogram based on the bins array.
    public static void printHistogramVertical(){
        
    }
}
