/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
import java.util.Scanner;
public class GradesAverage {
    public static void main (String [] args){
        
        int numStudents = 0;
        //int grades [] = new int [numStudents];
        double average;
        int sum = 0;
        
        Scanner in = new Scanner (System.in);
        System.out.print("Enter the number of students: ");
        numStudents = in.nextInt();
        int [] grades = new int [numStudents];
        
        for (int idx = 0; idx < numStudents; idx++){
            System.out.print("Enter the grade for student "+(idx+1)+": ");
            grades[idx] = in.nextInt();
            
            while (grades[idx] <= 0 || grades[idx] >= 100){
                System.out.println("Invalid grade, try again..");
                System.out.print("Enter the grade for student "+(idx+1)+": ");
                grades[idx] = in.nextInt();
            }
        sum += grades[idx];
        }
        average = (double) sum/numStudents;
        System.out.println("The average is "+average);
    }
}
