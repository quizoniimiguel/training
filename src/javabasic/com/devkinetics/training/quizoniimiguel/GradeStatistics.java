/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
import java.util.Scanner;
public class GradeStatistics {
    
    public static int[] grades;
    public static int numStudents;
    public static double ave;
    
    public static void main(String[] args){
        
        
        readGrades();
        System.out.println("The average is " + average());
        System.out.println("The minimum is " + min());
        System.out.println("The maximum is " + max());
        System.out.println("The standard deviation is " + stdDev());
    }
    
    public static void readGrades(){
                
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the number of students: ");
        numStudents = in.nextInt();
        grades = new int [numStudents];
        
        for (int idx = 0; idx < numStudents; idx++){
            
            System.out.print("Enter the grade of student "+(idx+1)+": ");
            grades[idx] = in.nextInt();
            
            while(grades[idx] <= 0 || grades[idx] >= 100){
                System.out.println("Invalid grade, please try again!");
                System.out.print("Enter the grade of student "+(idx+1)+": ");
                grades[idx] = in.nextInt();
            }            
        }        
    }
    
    public static double average(){
        
        int sum = 0;
        for (int idx = 0; idx < numStudents; idx++){
        sum += grades[idx];    
        }
        ave = sum/numStudents;
        return ave;
    }
    
    public static int max(){
        int maxGrade = 0;
        for (int idx = 0; idx < numStudents; idx++){
           if (grades[idx] > maxGrade){
               maxGrade = grades[idx];
           }
       }
       return maxGrade;
    }
    
    public static int min(){
        int minGrade = max();
        for (int idx = 0; idx < numStudents; idx++){
            if (grades[idx] < minGrade){
                minGrade = grades[idx];
            }
        }
        return minGrade;
    }
    
    public static double stdDev(){
        double dbStandDevia;
        int summation = 0;
        double SquaredDif;
        double dif = 0;
        double averageVal = average();
        double radicand = 0.0;
        for (int idx = 0; idx < numStudents; idx++){
            dif = grades[idx] - averageVal;
            SquaredDif = dif*dif;
            summation += SquaredDif;
            radicand = summation / numStudents;
        }
        dbStandDevia = Math.sqrt(radicand);
        return dbStandDevia;
        
    }    
}
