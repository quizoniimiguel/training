/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
public class SumAndAverageForLoop {
    public static void main (String [] args){
        int lowerbound = 111;
        int upperbound = 8899;
        int sum = 0;
        double average = 0;
        int count = 0; //count the number within the range, init to 0
       
        
        for (int number = lowerbound; number <= upperbound; ++number){ //loop
            sum += number;
            count++;
        }
        System.out.println("The sum is: " + sum);
        average = (double) sum/count;
        System.out.println("The average is: " + average);
    }
}
