/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
public class HarmonicSum {
    public static void main (String [] args){
        int maxDenominator = 50000;
        double sumLeftToRight = 0.0;
        double sumRightToLeft = 0.0;
        
        for (int denominator = 1; denominator <= maxDenominator; ++denominator){
            sumLeftToRight += ((double) 1 / denominator );
        }
        
        for (int denominator =1; denominator <= maxDenominator; ++denominator){ 
            sumRightToLeft += ((double) 1 /maxDenominator);
            --maxDenominator;
        }
        
        System.out.println("The sum from left to right is: " + sumLeftToRight);
        System.out.println("The sum from right to left is: " + sumRightToLeft);
        System.out.println("The difference of these two is: " + (sumLeftToRight - sumRightToLeft));
        
    }        
}
