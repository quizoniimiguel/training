/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
public class SumAndAverage { //Class for Sum and Average
    public static void main (String [] args) {
        int sum = 0; // for accumulation of the sum
        double average; // average in double
        int lowerbound = 1; // the lower bound to sum
        int upperbound = 100; // the upper bound to sum
        
        for (int number = lowerbound; number <= upperbound; ++number){ //loop
            sum += number;
        }
        System.out.println(sum);
        average = (double) sum;
        average = average/100;
        System.out.println(average);
    }
}
