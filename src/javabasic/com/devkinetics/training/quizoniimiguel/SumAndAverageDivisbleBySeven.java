/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
public class SumAndAverageDivisbleBySeven {
    public static void main (String [] args){
        int lowerbound = 1;
        int upperbound = 100;
        int sum = 0;
        double average = 0;
        int count = 0;
                
        for (int number = lowerbound; number <= upperbound; ++number ){
            if ((number % 7) == 0){
                sum += number;
            count++;                                                                                                                               
            }
        }
        System.out.println("The sum is: " + sum);
        average = (double) sum / count;
        System.out.println("The average is: " + average);
        
    }
}
