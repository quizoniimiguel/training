/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
import java.util.Scanner;

public class CircleComputation {
        public static void main (String [] args){
            
            double radius;
            double area;
            double perimeter;
            
            Scanner in = new Scanner (System.in);
            System.out.print("Enter the radius :");
            radius = in.nextDouble();
            area = (Math.PI*radius*radius);
            perimeter =(2*Math.PI*radius);
            System.out.println("The area is : "+area);
            System.out.println("The perimeter is :" +perimeter);
            
        }
    
}
