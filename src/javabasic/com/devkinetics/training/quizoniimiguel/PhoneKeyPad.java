/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */

import java.util.Scanner;

public class PhoneKeyPad {
    
    public static void main (String [] args){
         
        

         String inStr;   
         Scanner in = new Scanner (System.in);
         System.out.print("Enter a string : ");
         inStr = in.next().toLowerCase();
         
         int truelength = inStr.length() - 1;
         
         for (int counter = 0; counter <= truelength; counter++){
            char currentChar = inStr.charAt(counter);
      
             if ((currentChar == 'a' )||(currentChar == 'b')||(currentChar == 'c')){
                 System.out.print(2);
             } else if  ((currentChar == 'd' )||(currentChar == 'e')||(currentChar == 'f')){
                 System.out.print(3);
             } else if  ((currentChar == 'h' )||(currentChar == 'i')||(currentChar == 'j')){
                 System.out.print(4);
             } else if  ((currentChar == 'k' )||(currentChar == 'l')||(currentChar == 'm')){
                 System.out.print(5);
             } else if ((currentChar == 'n' )||(currentChar == 'o')||(currentChar == 'p')){
                 System.out.print(6);
             } else if ((currentChar == 'q' )||(currentChar == 'r')||(currentChar == 's')){
                 System.out.print(7);
             } else if  ((currentChar == 't' )||(currentChar == 'u')||(currentChar == 'v')){
                 System.out.print(8);
             } else if  ((currentChar == 'w' )||(currentChar == 'x')||(currentChar == 'y')||(currentChar == 'z')){
                 System.out.print(9);
             } else {
                 System.out.println("Invalid Input");
                 counter = truelength+1;       
            }
       } 
    }
}