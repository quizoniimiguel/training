/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
import java.util.Scanner;
public class Hex2Dec {
    public static void main (String [] args){
        
        String hexStr;
        char hexChar;
        int hexStrLen;
        int dec = 0;
        
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a Hexadecimal string: ");
        hexStr = in.next();
        hexStrLen = hexStr.length();
        
        for (int idx = 0; idx < hexStrLen; idx++){
            
            hexChar = hexStr.charAt(idx);
            int power = (int) Math.pow(16,idx);
            
            if (hexChar >= '1' && hexChar <= '9'){
                dec += (hexChar - '0') * power;
            } else if (hexChar >= 'a' && hexChar <= 'f'){
                dec += (hexChar - 'a' + 10) * power;
            } else if (hexChar >= 'A' && hexChar <= 'F'){
                dec += (hexChar - 'A' + 10) * power;
            } else {
                System.out.println("Error: Invalid hex String "+hexStr);
                System.exit(1);
            }
        }        
        System.out.println("The equivalent decimal for "+hexStr+" is "+ dec);
    }
}