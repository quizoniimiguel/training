/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
public class FactorialInt {
    public static void main (String[]args){
        for (int idx = 1; idx <= 10; idx++){
            System.out.println("The factorial of "+idx+" is "+factorial(idx));
            factorial(idx);
        }    
    }
    
    public static int factorial(int factor){
        int product = 1;
        while(factor != 0){
            product = product*factor;
            factor--;
        }
        return product;
    }
}
