/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;


/**
 *
 * @author devkinetics
 */
public class CheckOddEven { //saved as "CheckOddEven.java
    public static void main (String [] args){
        int number = 49; //set the value of number
        System.out.println("The number is " + number);
        if ((number % 2) == 0){
            System.out.println("This number is EVEN");
        } else {
            System.out.println("This number is ODD");
        }
    }
    
}
