/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
public class SumAndAverageOddOnly {
    public static void main (String [] args){
        int lowerbound = 1;
        int upperbound = 100;
        int sum = 0;
        double average = 0;
        int count = 0;
        
        for (int number = lowerbound; number <= 100; ++number){
            if ((number % 2) != 0){
                sum += number;
                count++;
            }           
        }
         System.out.println("The sum of odd numbers: " + sum);
         average = (double) sum / count;
         System.out.println("The average of odd numbers: " + average);
    }
    
}
