/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
public class ComputePI {
    public static void main (String [] args){
        double sum = 0;
        int maxDenom = 100000;
        double mod = 0;
        for (int denom = 1; denom <= maxDenom; denom = denom + 2){
            
            if (denom % 2 != 0){
                mod = denom;
            }            
            if (denom % 4 == 1){
                sum += 4*1/mod;  
            } else if (denom % 4 == 3){                
                sum -= 4*1/mod;
            } else {
               System.out.println("The computer has gone crazy?");
            }
            
        }
        
        System.out.println("The value of Math.PI is " + Math.PI);
        System.out.println("The value of Pi in the expansion series is " + (double)sum);
    }
    
}
