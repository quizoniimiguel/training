/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */

import java .util.Scanner;

public class ExercisesKeyBoardScanner {
    public static void main (String [] args){
        int num1;
        double num2;
        String name;
        double sum;
        
        Scanner in = new Scanner(System.in);
        System.out.print("Enter an integer: ");
        num1 = in.nextInt();
        System.out.print("Enter a floating point number: ");
        num2 = in.nextDouble();
        System.out.print("Enter yout name: ");
        name = in.next();
        
        System.out.println("Hi! "+name+", the sum of "+num1+" and "+num2+" is "+(num1+num2));
    }
    
}
