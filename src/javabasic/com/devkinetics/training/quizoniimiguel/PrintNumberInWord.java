/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;


/**
 *
 * @author devkinetics
 */
public class PrintNumberInWord {
    public static void main (String [] args) {
        int number = 3;
        
        if (number == 1){
            System.out.println("ONE");
        } else if(number == 2) {
            System.out.println("TWO");
        } else if (number == 3){
            System.out.println("THREE");
        } else if (number == 4){
            System.out.println("FOUR");
        } else if (number == 5){
            System.out.println("Five");
        } else {
            System.out.println("OTHER");
        }
    }
}
