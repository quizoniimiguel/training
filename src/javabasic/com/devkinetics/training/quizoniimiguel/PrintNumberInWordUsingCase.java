/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;


/**
 *
 * @author devkinetics
 */
public class PrintNumberInWordUsingCase {
    public static void main (String [] args) {
        int number = 5;
        
        switch (number) {
            case 1: System.out.println("ONE");break;
            case 2: System.out.println("TWO");break;
            case 3: System.out.println("THREE");break;
            case 4: System.out.println("FOUR");break;
            case 5: System.out.println("FIVE");break;
            case 6: System.out.println("SIX");break;
            default: System.out.println("OTHER");
        }
    }
}
