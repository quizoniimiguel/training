/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
public class SumAndAverageSumOfTheSquares {
    public static void main (String [] args){
        
        int sum = 0;
        for (int number = 1; number <= 5; number++){
            sum += number*number;
        }
        System.out.println("The sum of the squares :" + sum);
    }    
}
