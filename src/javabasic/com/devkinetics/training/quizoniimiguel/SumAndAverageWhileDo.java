/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
public class SumAndAverageWhileDo {
    public static void main (String [] args){
        int sum = 0;
        double average;
        int lowerbound = 1;
        int upperbound = 100;
        int number = lowerbound;
        
        while (number <= upperbound){
            sum += number;
            ++number;
        }
        
        System.out.println("The sum is: " + sum);
        average = (double) sum/ 100;
        System.out.println("The average is: " + average);
    }
    
}
