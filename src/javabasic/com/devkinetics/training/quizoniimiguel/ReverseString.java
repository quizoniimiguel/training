/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
import java.util.Scanner;
public class ReverseString {
    public static void main (String [] args){
        
        String inStr;
        int inStrLen;
        int index = 0;        
        int trueLength=0;
        String revStr = "";
        
        
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a string: ");
        inStr = in.next();
        inStrLen = inStr.length();
        trueLength = inStr.length()-1;
        
        while (trueLength >= 0){
               
            revStr = revStr+inStr.charAt(trueLength);
            
            trueLength--;               
        }
        
        System.out.println("The reverse of the string "+inStr+" is "+revStr);
        
    }
    
}
