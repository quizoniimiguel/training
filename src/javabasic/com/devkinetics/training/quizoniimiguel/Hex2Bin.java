/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
import java.util.Scanner;
public class Hex2Bin {
    public static void main (String [] args){
        
        String hexStr;
        int hexStrLen;
        String binStr =  "";
        
        String [] binStrs = {"0000","0001","0010","0011","0100","0101","0110","0111",
                             "1000","1001","1010","1011","1100","1101","1110","1111"};
        
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a Hexadecimal string: ");
        hexStr = in.next();
        hexStrLen = hexStr.length();
        
        for (int idx = 0; idx < hexStrLen; ++idx){
            char hexChar = hexStr.charAt(idx);
            if (hexChar >= '0' && hexChar <= '9'){
                binStr += binStrs[hexChar - '0'];
            } else if (hexChar >= 'a' && hexChar <= 'f'){
                binStr += binStrs[hexChar - 'a' + 10];
            } else if (hexChar >= 'A' && hexChar <= 'F'){
                binStr += binStrs[hexChar - 'A' + 10];
            } else {
                System.err.println("Error: Invalid Hex String "+hexStr);
                System.exit(1);
            }
        } 
        System.out.println("The equivalent binary for "+hexStr+"is "+binStr);
    }
}
