/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javabasic.com.devkinetics.training.quizoniimiguel;

/**
 *
 * @author devkinetics
 */
import java.util.Scanner;

public class Bin2Dec {
    public static void main (String [] args){
        
        String binStr;
        int binStrLen;
        int dec = 0;
        char binChar;
        int idx = 0;
        int bits;
        Scanner in = new Scanner (System.in);
        System.out.print("Enter a Binary string: ");
        binStr = in.next();
        binStrLen = binStr.length();
        
        for (idx = 0; idx < binStrLen; idx++){
            binChar = binStr.charAt(idx);
            bits = binChar - '0';
            
            if (bits == 1){                
                dec += (int)Math.pow(2, idx);
            } else if (bits != 0) {
                System.err.println("Invalid Binary String: "+binStr);
                System.exit(0);
            }
        }
        
        System.out.println("The equivalent decimal number for binary "+binStr+" is "+dec);
        
    }
} 
